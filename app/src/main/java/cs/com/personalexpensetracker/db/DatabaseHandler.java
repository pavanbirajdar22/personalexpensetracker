package cs.com.personalexpensetracker.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import cs.com.personalexpensetracker.entities.Expense;

/**
 * Created by Pavan on 06/07/16.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "ExpenseTracker";

    // Expenses table name
    private static final String TABLE_EXPENSES = "expenses";

    // Expenses Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_AMT = "amt";
    private static final String KEY_TIMESTAMP = "timestamp";
    private static final String KEY_NOTE = "note";
    private String TAG = "pet_tag";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Create a helper object to create, open, and/or manage a database.
     * This method always returns very quickly.  The database is not actually
     * created or opened until one of {@link #getWritableDatabase} or
     * {@link #getReadableDatabase} is called.
     *
     * @param context to use to open or create the database
     * @param name    of the database file, or null for an in-memory database
     * @param factory to use for creating cursor objects, or null for the default
     * @param version number of the database (starting at 1); if the database is older,
     *                {@link #onUpgrade} will be used to upgrade the database; if the database is
     *                newer, {@link #onDowngrade} will be used to downgrade the database
     */
    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * Create a helper object to create, open, and/or manage a database.
     * The database is not actually created or opened until one of
     * {@link #getWritableDatabase} or {@link #getReadableDatabase} is called.
     * <p>
     * <p>Accepts input param: a concrete instance of {@link DatabaseErrorHandler} to be
     * used to handle corruption when sqlite reports database corruption.</p>
     *
     * @param context      to use to open or create the database
     * @param name         of the database file, or null for an in-memory database
     * @param factory      to use for creating cursor objects, or null for the default
     * @param version      number of the database (starting at 1); if the database is older,
     *                     {@link #onUpgrade} will be used to upgrade the database; if the database is
     *                     newer, {@link #onDowngrade} will be used to downgrade the database
     * @param errorHandler the {@link DatabaseErrorHandler} to be used when sqlite reports database
     */
    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG,"DB onCreate");
        String CREATE_EXPENSES_TABLE = "CREATE TABLE " + TABLE_EXPENSES + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_CATEGORY + " TEXT NOT NULL,"
                + KEY_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
                + KEY_NOTE + " TEXT NOT NULL DEFAULT 'N/A',"
                + KEY_AMT + " REAL" + ")";
        db.execSQL(CREATE_EXPENSES_TABLE);
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG,"onUpgrade");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSES);
        onCreate(db);
    }

    public long addExpense(Expense expense) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY, expense.getCategory());
        values.put(KEY_AMT, expense.getAmt());
        if (expense.getNote().length()>0) {
            values.put(KEY_NOTE,expense.getNote());
        }
        // Inserting Row
        long res = db.insert(TABLE_EXPENSES, null, values);
        db.close(); // Closing database connection
        return res;
    }

    public ArrayList<Expense> getAllExpenses() {
        ArrayList<Expense> expenses = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_EXPENSES+" ORDER BY "+KEY_TIMESTAMP+" DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Expense expense = new Expense();

                expense.setAmt(cursor.getDouble(cursor.getColumnIndex(KEY_AMT)));
                expense.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
                expense.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                expense.setTimestamp(cursor.getString(cursor.getColumnIndex(KEY_TIMESTAMP)));
                expense.setNote(cursor.getString(cursor.getColumnIndex(KEY_NOTE)));

                expenses.add(expense);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return expenses;
    }

    public Expense getExpense(long id) {
        Expense expense = new Expense();
        String selectQuery = "SELECT  * FROM " + TABLE_EXPENSES + " WHERE "+KEY_ID+"="+id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                expense.setAmt(cursor.getDouble(cursor.getColumnIndex(KEY_AMT)));
                expense.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY)));
                expense.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                expense.setTimestamp(cursor.getString(cursor.getColumnIndex(KEY_TIMESTAMP)));
                expense.setNote(cursor.getString(cursor.getColumnIndex(KEY_NOTE)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        Log.d(TAG,expense.toString());
        db.close();
        return expense;
    }

    public int deleteAllExpenses() {
        SQLiteDatabase db = this.getWritableDatabase();
        int count = db.delete(TABLE_EXPENSES,null,null);
        Log.d(TAG,""+count+" expenses deleted from database.");
        return count;
    }

    public int deleteExpense(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = KEY_ID+" = "+id;
        int i = db.delete(TABLE_EXPENSES,whereClause,null);
        Log.d(TAG,""+i+" Expense deleted");
        db.close();
        return i;
    }

    public int updateExpense(Expense expense) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_AMT,expense.getAmt());
        contentValues.put(KEY_CATEGORY,expense.getCategory());
        contentValues.put(KEY_TIMESTAMP,expense.getTimestamp());
        contentValues.put(KEY_NOTE,expense.getNote());

        String whereClause = KEY_ID+" = "+expense.getId();
        int count = db.update(TABLE_EXPENSES, contentValues, whereClause, null);
        Log.d(TAG,""+count+" Expense updated");
        db.close();
        return count;
    }

    public HashMap<String,Double> getExpenditureByCategory(){
        HashMap<String,Double> hashMap = new HashMap<>();
        String query = "SELECT "+KEY_CATEGORY+", SUM("+KEY_AMT+") AS Amount FROM " + TABLE_EXPENSES+" GROUP BY "+KEY_CATEGORY;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        String cat;
        double a;
        if (cursor.moveToFirst()) {
            do {
                cat = cursor.getString(cursor.getColumnIndex(KEY_CATEGORY));
                a = cursor.getDouble(cursor.getColumnIndex("Amount"));
                hashMap.put(cat,a);
            } while (cursor.moveToNext());
        }
        else {
            return null;
        }
        Log.d(TAG,hashMap.toString());
        cursor.close();
        db.close();
        return hashMap;
    }
}
