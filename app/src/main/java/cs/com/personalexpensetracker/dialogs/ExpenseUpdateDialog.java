package cs.com.personalexpensetracker.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import cs.com.personalexpensetracker.R;
import cs.com.personalexpensetracker.db.DatabaseHandler;
import cs.com.personalexpensetracker.entities.Expense;
import cs.com.personalexpensetracker.fragments.ExpenseHistoryFragment;

/**
 * Created by Pavan on 11/07/16.
 */
public class ExpenseUpdateDialog extends Dialog{
    EditText amtET,noteET;
    Spinner spinner;
    Button updateBtn;
    Expense expense;
    int selectedViewExpensePosition;
    /**
     * Creates a dialog window that uses the default dialog theme.
     * <p/>
     * The supplied {@code context} is used to obtain the window manager and
     * base theme used to present the dialog.
     *
     * @param context the context in which the dialog should run
     */
    public ExpenseUpdateDialog(Context context) {
        super(context);
    }
    public ExpenseUpdateDialog(Context context,Expense expense,int selectedViewExpensePosition) {
        super(context);
        this.expense = expense;
        this.selectedViewExpensePosition = selectedViewExpensePosition;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_expense_update);
        spinner = (Spinner) findViewById(R.id.dialog_item_category);
        amtET = (EditText) findViewById(R.id.dialog_item_amt);
        noteET = (EditText) findViewById(R.id.dialog_item_note);
        updateBtn = (Button) findViewById(R.id.dialog_item_update_btn);

        amtET.setText(String.valueOf(expense.getAmt()));
        noteET.setText(expense.getNote());

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.expense_categories, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        int position = adapter.getPosition(expense.getCategory());
        spinner.setSelection(position);

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (amtET.getText().toString().length() > 0) {
                    String n = noteET.getText().toString();
                    n = n.trim();
                    if (validateNote(n)) {
                        DatabaseHandler db = new DatabaseHandler(getContext());
                        expense.setCategory(spinner.getSelectedItem().toString());
                        expense.setAmt(Double.parseDouble(amtET.getText().toString()));
                        expense.setTimestamp(expense.getTimestamp());
                        if (n.length()>0) {
                            expense.setNote(n);
                        }
                        else {
                            expense.setNote("N/A");
                        }

                        int count = db.updateExpense(expense);
                        if (count > 0) {
                            ExpenseHistoryFragment.expenses.set(selectedViewExpensePosition, expense);
                            ExpenseHistoryFragment.updateAdapter(expense,2);
                        } else {
                            Snackbar.make(v, "Error!", Snackbar.LENGTH_SHORT).show();
                        }
                        dismiss();
                        db.close();
                    }
                    else {
                        noteET.setError("Invalid input");
                    }
                }
                else {
                    amtET.setError("Amount required");
                }
            }
        });
    }

    private boolean validateNote(String s) {
        if (s.length()>0) {
            boolean b = s.matches("[a-zA-Z0-9./? ]*");
            return b;
        }
        else return true;
    }
}
