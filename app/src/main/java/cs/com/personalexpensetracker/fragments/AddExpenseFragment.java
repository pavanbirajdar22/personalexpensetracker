package cs.com.personalexpensetracker.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import cs.com.personalexpensetracker.R;
import cs.com.personalexpensetracker.db.DatabaseHandler;
import cs.com.personalexpensetracker.entities.Expense;

/**
 * Created by Pavan on 05/07/16.
 */
public class AddExpenseFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    Spinner spinner;
    Button addBtn;
    EditText amtET,noteET;
    private String TAG = "pet_tag";

    public AddExpenseFragment() {

    }

    public static AddExpenseFragment newInstance(int sectionNumber) {
        AddExpenseFragment fragment = new AddExpenseFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_expense, container, false);
        Log.d(TAG, "ARG_SECTION_NUMBER : " + ARG_SECTION_NUMBER);
        return rootView;
    }

    /**
     * Called immediately after {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * has returned, but before any saved state has been restored in to the view.
     * This gives subclasses a chance to initialize themselves once
     * they know their view hierarchy has been completely created.  The fragment's
     * view hierarchy is not however attached to its parent at this point.
     *
     * @param view               The View returned by {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        addBtn = (Button) getView().findViewById(R.id.addBtn);
        spinner = (Spinner) getView().findViewById(R.id.spinner);
        amtET = (EditText) getView().findViewById(R.id.et_expense_amt);
        noteET = (EditText) getView().findViewById(R.id.et_expense_note);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.expense_categories, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (amtET.getText().toString().length() > 0) {
                    String n = noteET.getText().toString();
                    n = n.trim();
                    if (validateNote(n)) {
                        Log.d(TAG,"Note : "+n);
                        DatabaseHandler db = new DatabaseHandler(getContext());
                        Expense expense = new Expense();
                        expense.setCategory(spinner.getSelectedItem().toString());
                        expense.setAmt(Double.parseDouble(amtET.getText().toString()));
                        expense.setNote(n);

                        long id = db.addExpense(expense);
                        if (id > 0) {
                            Snackbar.make(v, "Expense added", Snackbar.LENGTH_SHORT).show();
                            Expense newExpense = db.getExpense(id);
                            ExpenseHistoryFragment.updateAdapter(newExpense,1);
                        } else {
                            Snackbar.make(v, "Error!", Snackbar.LENGTH_SHORT).show();
                        }
                        db.close();
                    }
                    else {
                        noteET.setError("Invalid input");
                    }
                }
                else {
                    amtET.setError("Amount required");
                }
                noteET.setText("");
                amtET.setText("");
            }
        });

    }

    private boolean validateNote(String s) {
        if (s.length()>0) {
            boolean b = s.matches("[a-zA-Z0-9.? ]*");
            return b;
        }
        else return true;
    }
}
