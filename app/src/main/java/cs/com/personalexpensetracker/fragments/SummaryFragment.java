package cs.com.personalexpensetracker.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.HashMap;

import cs.com.personalexpensetracker.R;
import cs.com.personalexpensetracker.db.DatabaseHandler;

/**
 * Created by Pavan on 08/07/16.
 */
public class SummaryFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private String TAG = "pet_tag";
    private PieChart pieChart;
    private PieDataSet pieDataSet;
    private PieData data;
    private HashMap<String, Double> expenditureByCategory;
    private ArrayList<PieEntry> entries;

    private int[] myColors = {Color.parseColor("#E91E63"),
            Color.parseColor("#f44336"),
            Color.parseColor("#9C27B0"),
            Color.parseColor("#03A9F4"),
            Color.parseColor("#4CAF50"),
            Color.parseColor("#FF9800"),
            Color.parseColor("#795548"),
            Color.parseColor("#009688")
    };

    public SummaryFragment() {
    }

    public static SummaryFragment newInstance(int sectionNumber) {
        SummaryFragment fragment = new SummaryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_summary, container, false);
        Log.d(TAG, "ARG_SECTION_NUMBER : " + ARG_SECTION_NUMBER);
        return rootView;
    }

    /**
     * Called immediately after {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * has returned, but before any saved state has been restored in to the view.
     * This gives subclasses a chance to initialize themselves once
     * they know their view hierarchy has been completely created.  The fragment's
     * view hierarchy is not however attached to its parent at this point.
     *
     * @param view               The View returned by {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pieChart = (PieChart) getView().findViewById(R.id.chart);
        pieChart.setCenterText("CATEGORY WISE EXPENDITURE");
        pieChart.setCenterTextTypeface(Typeface.DEFAULT_BOLD);
        pieChart.setEntryLabelColor(Color.BLACK);
        pieChart.setCenterTextColor(Color.parseColor("#303F9F"));
        pieChart.setSoundEffectsEnabled(true);
        pieChart.setDescription("");

        entries = new ArrayList<>();

        DatabaseHandler databaseHandler = new DatabaseHandler(getContext());
        expenditureByCategory = databaseHandler.getExpenditureByCategory();

        if (expenditureByCategory!=null) {
            for (String s : expenditureByCategory.keySet()) {
                entries.add(new PieEntry(expenditureByCategory.get(s).floatValue(),s));
            }
            pieDataSet = new PieDataSet(entries,"Categories");
            pieDataSet.setColors(myColors); // set the color
            data = new PieData(pieDataSet); // initialize Piedata
            pieChart.setData(data);
        } else {
            pieChart.setNoDataText("NO EXPENDITURE YET!");
            pieChart.setNoDataTextColor(Color.BLACK);
            pieChart.setNoDataTextDescription("Add expenses to get summary view.");
        }
    }
}
