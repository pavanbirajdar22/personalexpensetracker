package cs.com.personalexpensetracker.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cs.com.personalexpensetracker.R;
import cs.com.personalexpensetracker.db.DatabaseHandler;
import cs.com.personalexpensetracker.dialogs.ExpenseUpdateDialog;
import cs.com.personalexpensetracker.entities.Expense;
import cs.com.personalexpensetracker.fragments.decoration.DividerItemDecoration;

/**
 * Created by Pavan on 05/07/16.
 */
public class ExpenseHistoryFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private String TAG = "pet_tag";
    public static List<Expense> expenses;
    private RecyclerView recyclerView;
    public static ExpensesAdapter mAdapter;
    private static long selectedViewExpenseId;
    private static int selectedViewExpensePosition;
    private static Expense selectedViewExpense;

    public ExpenseHistoryFragment() {
    }

    public static ExpenseHistoryFragment newInstance(int sectionNumber) {
        ExpenseHistoryFragment fragment = new ExpenseHistoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER,sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_expense_history, container, false);
        return rootView;
    }

    /**
     * Called immediately after {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * has returned, but before any saved state has been restored in to the view.
     * This gives subclasses a chance to initialize themselves once
     * they know their view hierarchy has been completely created.  The fragment's
     * view hierarchy is not however attached to its parent at this point.
     *
     * @param view               The View returned by {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        DatabaseHandler db = new DatabaseHandler(getContext());
        ArrayList<Expense> allExpenses = db.getAllExpenses();
        expenses = allExpenses;
        mAdapter = new ExpensesAdapter();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Log.d(TAG,"onClick");
                Expense expense = expenses.get(position);
                new AlertDialog.Builder(getContext())
                        .setTitle("Expense details")
                        .setMessage("ID : "+expense.getId()+
                                "\nAMOUNT : "+expense.getAmt()+
                                "\nCATEGORY : "+expense.getCategory()+
                                "\nDATE & TIME : "+expense.getTimestamp()+
                                "\nNOTE : "+expense.getNote()
                        )
                        .show()
                ;
            }

            @Override
            public void onLongClick(View view, int position) {
                Log.d(TAG,"onLongClick");
                Expense expense = expenses.get(position);
                selectedViewExpense = expense;
                selectedViewExpensePosition = position;
                selectedViewExpenseId = expense.getId();
            }
        }));

    }

    //Send null if all expenses are deleted from database
    public static void updateAdapter(Expense expense,int crud){
        if (expense != null) {
            switch (crud) {
                case 1 :
                    expenses.add(0,expense);
                    break;
                case 2 :
                    expenses.set(selectedViewExpensePosition,expense);
                    break;
            }
        }
        else {
            expenses.clear();
        }
        mAdapter.notifyDataSetChanged();
    }

    class ExpensesAdapter extends RecyclerView.Adapter<ExpensesAdapter.MyViewHolder> {
        public ExpensesAdapter() {
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.expense_item, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            Expense expense = expenses.get(position);
            String s = String.valueOf(expense.getAmt())+" "+getResources().getString(R.string.rs);
            holder.amt.setText(s);
            holder.category.setText(expense.getCategory());
            holder.timestamp.setText(expense.getTimestamp());
        }

        @Override
        public int getItemCount() {
            return expenses.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder
                implements View.OnCreateContextMenuListener {
            public TextView amt, timestamp, category;
            public MyViewHolder(View itemView) {
                super(itemView);
                amt = (TextView) itemView.findViewById(R.id.item_amt);
                timestamp = (TextView) itemView.findViewById(R.id.item_time);
                category = (TextView) itemView.findViewById(R.id.item_category);

                itemView.setOnCreateContextMenuListener(this);
            }

            /**
             * Called when the context menu for this view is being built. It is not
             * safe to hold onto the menu after this method returns.
             *
             * @param menu     The context menu that is being built
             * @param v        The view for which the context menu is being built
             * @param menuInfo Extra information about the item for which the
             *                 context menu should be shown. This information will vary
             */
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                MenuInflater menuInflater = new MenuInflater(getContext());
                menuInflater.inflate(R.menu.menu_item,menu);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete_item) {
            final DatabaseHandler db = new DatabaseHandler(getContext());
            new AlertDialog.Builder(getContext())
                    .setTitle("Confirm your action")
                    .setMessage("Delete expense")
                    .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            db.deleteExpense(selectedViewExpenseId);
                            expenses.remove(selectedViewExpensePosition);
                            mAdapter.notifyItemRemoved(selectedViewExpensePosition);
                            Toast.makeText(getContext(),"Expense deleted",Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setNegativeButton("CANCEL",null)
                    .show();
        }
        if (id == R.id.action_share_item) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Expense details");
            sendIntent.putExtra(Intent.EXTRA_TEXT, expenses.get(selectedViewExpensePosition).getShareText());
            startActivity(sendIntent);
        }
        if (id == R.id.action_update_item) {
            ExpenseUpdateDialog expenseUpdateDialog = new ExpenseUpdateDialog(getContext(),expenses.get(selectedViewExpensePosition),selectedViewExpensePosition);
            expenseUpdateDialog.show();
            Log.d(TAG,"Updated");
        }
        return super.onContextItemSelected(item);
    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {
        private GestureDetector gestureDetector;
        private ClickListener clickListener;
        /**
         * Silently observe and/or take over touch events sent to the RecyclerView
         * before they are handled by either the RecyclerView itself or its child views.
         * <p/>
         * <p>The onInterceptTouchEvent methods of each attached OnItemTouchListener will be run
         * in the order in which each listener was added, before any other touch processing
         * by the RecyclerView itself or child views occurs.</p>
         *
         * @param rv
         * @param e  MotionEvent describing the touch event. All coordinates are in
         *           the RecyclerView's coordinate system.
         * @return true if this OnItemTouchListener wishes to begin intercepting touch events, false
         * to continue with the current behavior and continue observing future events in
         * the gesture.
         */
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());

            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;

            gestureDetector = new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        /**
         * Process a touch event as part of a gesture that was claimed by returning true from
         * a previous call to {@link #onInterceptTouchEvent}.
         *
         * @param rv
         * @param e  MotionEvent describing the touch event. All coordinates are in
         */
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        /**
         * Called when a child of RecyclerView does not want RecyclerView and its ancestors to
         * intercept touch events with
         * {@link ViewGroup#onInterceptTouchEvent(MotionEvent)}.
         *
         * @param disallowIntercept True if the child does not want the parent to
         *                          intercept touch events.
         * @see ViewParent#requestDisallowInterceptTouchEvent(boolean)
         */
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
