package cs.com.personalexpensetracker;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import cs.com.personalexpensetracker.db.DatabaseHandler;
import cs.com.personalexpensetracker.fragments.AddExpenseFragment;
import cs.com.personalexpensetracker.fragments.ExpenseHistoryFragment;
import cs.com.personalexpensetracker.fragments.SummaryFragment;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "pet_tag";
    /**
     * The {android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(1,false);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, R.string.coming_soon, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(this, R.string.coming_soon, Toast.LENGTH_SHORT).show();
        }
        if (id == R.id.action_aboutus) {
            Toast.makeText(this, R.string.coming_soon, Toast.LENGTH_SHORT).show();
        }
        if (id == R.id.action_clearData) {
            final DatabaseHandler db = new DatabaseHandler(this);
            new AlertDialog.Builder(this)
                    .setTitle("Confirm your action")
                    .setMessage("Delete all data")
                    .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            db.deleteAllExpenses();
                            Toast.makeText(getBaseContext(), "All data cleared!", Toast.LENGTH_SHORT).show();
                            ExpenseHistoryFragment.updateAdapter(null,0);
                        }
                    })
                    .setNegativeButton("CANCEL",null)
                    .show();
        }
        if (id == R.id.action_share_app) {
            String shareText = "Coming soon on Play Store!";
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Expense Tracker Play Store URL");
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
            startActivity(sendIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A {FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Log.d(TAG, "Position : " + position);
            switch (position) {
                case 0:
                    return SummaryFragment.newInstance(position + 1);
                case 1:
                    return AddExpenseFragment.newInstance(position + 1);
                case 2:
                    return ExpenseHistoryFragment.newInstance(position + 1);
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            //Log.d(TAG,"Page count : "+2);
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Summary";
                case 1:
                    return "Add Expense";
                case 2:
                    return "History";
            }
            return null;
        }
    }
}
