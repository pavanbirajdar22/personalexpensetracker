package cs.com.personalexpensetracker.entities;

/**
 * Created by Pavan on 06/07/16.
 */
public class Expense {
    private double amt;
    private String category;
    private int id;
    private String timestamp;
    private String note;

    public Expense() {
    }

    public Expense(double amt, String category, String timestamp) {
        this.amt = amt;
        this.category = category;
        this.timestamp = timestamp;
    }

    public Expense(double amt, String category, int id, String timestamp) {
        this.amt = amt;
        this.category = category;
        this.id = id;
        this.timestamp = timestamp;
    }

    public Expense(double amt, String category) {
        this.amt = amt;
        this.category = category;
    }

    public String getShareText() {
        return "Expense details "+"\n\n"+
                "AMOUNT : " + amt + "\n"+
                "CATEGORY : " + category + "\n"+
                "DATE & TIME : " + timestamp + "\n"+
                "NOTE : " + note;
    }

    @Override
    public String toString() {
        return "Expense{" +
                "amt=" + amt +
                ", category='" + category + '\'' +
                ", id=" + id +
                ", timestamp='" + timestamp + '\'' +
                ", note='" + note + '\'' +
                '}';
    }

    public double getAmt() {
        return amt;
    }

    public void setAmt(double amt) {
        this.amt = amt;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
